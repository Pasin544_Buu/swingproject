/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.swingproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Pla
 */
public class Computer {

    private int hand;
    private int playerHand, win, draw, lose,status,combo;

    public Computer() {

    }

    public int getHand() {
        return hand;
    }

    public void setHand(int hand) {
        this.hand = hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public void setPlayerHand(int playerHand) {
        this.playerHand = playerHand;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int choob(){
        return ThreadLocalRandom.current().nextInt(0, 3);
    }
    public int paoYingChoob(int playerHand) {
        this.playerHand = playerHand;
        this.hand = choob();
        if(this.playerHand == this.hand){
            draw++;
            status =0;
            return 0;
        }if((this.playerHand == 0 && this.hand==1) || (this.playerHand == 1 && this.hand==2) || (this.playerHand == 2 && this.hand==0)){
            win++;
            status =1;
            combo+=1;
            return 1;
            
        }
        lose++;
        combo=0;
        status = -1;
        return -1;
    }

    public int getCombo() {
        return combo;
    }

    public void setCombo(int combo) {
        this.combo = combo;
    }
    
}
